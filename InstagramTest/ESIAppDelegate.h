//
//  ESIAppDelegate.h
//  InstagramTest
//
//  Created by Ефанов Сергей on 12.09.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
