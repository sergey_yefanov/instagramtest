//
//  main.m
//  InstagramTest
//
//  Created by Ефанов Сергей on 12.09.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ESIAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([ESIAppDelegate class]));
	}
}
